"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var exec = require('cordova/exec');

var cordova = require('cordova');

var VERSION = '1.0.7';
/**
 * This enum represents whether or not the consent dialog should be shown for this user.
 * The state where no such determination could be made is represented by `Unknown`.
 */

var ConsentDialogState = {
  /**
   * The consent dialog state could not be determined. This is likely due to SDK failing to initialize.
   */
  UNKNOWN: 0,

  /**
   * This user should be shown a consent dialog.
   */
  APPLIES: 1,

  /**
   * This user should not be shown a consent dialog.
   */
  DOES_NOT_APPLY: 2
};
var AdFormat = {
  BANNER: 'banner',
  MREC: 'mrec'
};
var AdViewPosition = {
  TOP_CENTER: 'top_center',
  TOP_RIGHT: 'top_right',
  CENTERED: 'centered',
  CENTER_LEFT: 'center_left',
  CENTER_RIGHT: 'center_right',
  BOTTOM_LEFT: 'bottom_left',
  BOTTOM_CENTER: 'bottom_center',
  BOTTOM_RIGHT: 'bottom_right'
};

function isFunction(functionObj) {
  return typeof functionObj === 'function';
}

function callNative(name) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var successCallback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var errorCallback = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  cordova.exec(successCallback, errorCallback, 'AppLovinMAX', name, params);
}

var AppLovinMAX = {
  VERSION: VERSION,
  ConsentDialogState: ConsentDialogState,
  AdFormat: AdFormat,
  AdViewPosition: AdViewPosition,
  // NOTE: We have to store states in JS as workaround for callback-based API
  // since Cordova does not allow for synchronous returns
  initialized: false,
  hasUserConsentValue: null,
  isAgeRestrictedUserValue: null,
  isDoNotSellValue: null,
  isTabletValue: null,
  isAdReadyValues: {},
  initialize: function initialize(sdkKey, callback) {
    callNative('initialize', [VERSION, sdkKey], function (config) {
      AppLovinMAX.initialized = true;
      AppLovinMAX.hasUserConsentValue = config.hasUserConsent;
      AppLovinMAX.isAgeRestrictedUserValue = config.isAgeRestrictedUser;
      AppLovinMAX.isDoNotSellValue = config.isDoNotSell;
      AppLovinMAX.isTabletValue = config.isTablet;

      if (isFunction(callback)) {
        callback(config);
      }
    });
  },
  isInitialized: function isInitialized() {
    return this.initialized;
  },
  showMediationDebugger: function showMediationDebugger() {
    callNative('showMediationDebugger');
  },

  /*--------------*/

  /* PRIVACY APIs */

  /*--------------*/
  getConsentDialogState: function getConsentDialogState(callback) {
    // Always call native as state might change from UNKNOWN
    callNative('getConsentDialogState', [], callback);
  },
  setHasUserConsent: function setHasUserConsent(hasUserConsent) {
    this.hasUserConsentValue = hasUserConsent;
    callNative('setHasUserConsent', [hasUserConsent]);
  },
  hasUserConsent: function hasUserConsent() {
    return this.hasUserConsentValue;
  },
  setIsAgeRestrictedUser: function setIsAgeRestrictedUser(isAgeRestrictedUser) {
    this.isAgeRestrictedUserValue = isAgeRestrictedUser;
    callNative('setIsAgeRestrictedUser', [isAgeRestrictedUser]);
  },
  isAgeRestrictedUser: function isAgeRestrictedUser() {
    return this.isAgeRestrictedUserValue;
  },
  setDoNotSell: function setDoNotSell(isDoNotSell) {
    this.isDoNotSellValue = isDoNotSell;
    callNative('setDoNotSell', [isDoNotSell]);
  },
  isDoNotSell: function isDoNotSell() {
    return this.isDoNotSellValue;
  },

  /*--------------------*/

  /* GENERAL PUBLIC API */

  /*--------------------*/
  isTablet: function isTablet() {
    return this.isTabletValue;
  },
  setUserId: function setUserId(userId) {
    callNative('setUserId', [userId]);
  },
  setMuted: function setMuted(muted) {
    callNative('setMuted', [muted]);
  },
  setVerboseLogging: function setVerboseLogging(verboseLoggingEnabled) {
    callNative('setVerboseLogging', [verboseLoggingEnabled]);
  },
  setTestDeviceAdvertisingIds: function setTestDeviceAdvertisingIds(advertisingIds) {
    callNative('setTestDeviceAdvertisingIds', [advertisingIds]);
  },

  /*----------------*/

  /* EVENT TRACKING */

  /*----------------*/
  trackEvent: function trackEvent(event) {
    var parameters = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var parametersToSend = [event];

    if (parameters != null) {
      parametersToSend.push(parameters);
    }

    callNative('trackEvent', parametersToSend);
  },

  /*---------*/

  /* BANNERS */

  /*---------*/
  createBanner: function createBanner(adUnitId, bannerPosition) {
    callNative('createBanner', [adUnitId, bannerPosition]);
  },
  setBannerBackgroundColor: function setBannerBackgroundColor(adUnitId, hexColorCode) {
    callNative('setBannerBackgroundColor', [adUnitId, hexColorCode]);
  },
  setBannerPlacement: function setBannerPlacement(adUnitId, placement) {
    callNative('setBannerPlacement', [adUnitId, placement]);
  },
  updateBannerPosition: function updateBannerPosition(adUnitId, bannerPosition) {
    callNative('updateBannerPosition', [adUnitId, bannerPosition]);
  },
  setBannerExtraParameter: function setBannerExtraParameter(adUnitId, key, value) {
    callNative('setBannerExtraParameter', [adUnitId, key, value]);
  },
  showBanner: function showBanner(adUnitId) {
    callNative('showBanner', [adUnitId]);
  },
  hideBanner: function hideBanner(adUnitId) {
    callNative('hideBanner', [adUnitId]);
  },
  destroyBanner: function destroyBanner(adUnitId) {
    callNative('destroyBanner', [adUnitId]);
  },

  /*-------*/

  /* MRECS */

  /*-------*/
  createMRec: function createMRec(adUnitId, mrecPosition) {
    callNative('createMRec', [adUnitId, mrecPosition]);
  },
  setMRecBackgroundColor: function setMRecBackgroundColor(adUnitId, hexColorCode) {
    callNative('setMRecBackgroundColor', [adUnitId, hexColorCode]);
  },
  setMRecPlacement: function setMRecPlacement(adUnitId, placement) {
    callNative('setMRecPlacement', [adUnitId, placement]);
  },
  updateMRecPosition: function updateMRecPosition(adUnitId, mrecPosition) {
    callNative('updateMRecPosition', [adUnitId, mrecPosition]);
  },
  setMRecExtraParameter: function setMRecExtraParameter(adUnitId, key, value) {
    callNative('setMRecExtraParameter', [adUnitId, key, value]);
  },
  showMRec: function showMRec(adUnitId) {
    callNative('showMRec', [adUnitId]);
  },
  hideMRec: function hideMRec(adUnitId) {
    callNative('hideMRec', [adUnitId]);
  },
  destroyMRec: function destroyMRec() {
    callNative('destroyMRec', [adUnitId]);
  },

  /*---------------*/

  /* INTERSTITIALS */

  /*---------------*/
  loadInterstitial: function loadInterstitial(adUnitId) {
    callNative('loadInterstitial', [adUnitId]);
  },
  isInterstitialReady: function isInterstitialReady(adUnitId) {
    var isReady = this.isAdReadyValues[adUnitId];
    return typeof isReady === 'boolean' && isReady === true;
  },
  showInterstitial: function showInterstitial(adUnitId) {
    var placement = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var parameters = [adUnitId];

    if (placement != null) {
      parameters.push(placement);
    }

    callNative('showInterstitial', parameters);
  },
  setInterstitialExtraParameter: function setInterstitialExtraParameter(adUnitId, key, value) {
    callNative('setInterstitialExtraParameter', [adUnitId, key, value]);
  },

  /*----------*/

  /* REWARDED */

  /*----------*/
  loadRewardedAd: function loadRewardedAd(adUnitId) {
    callNative('loadRewardedAd', [adUnitId]);
  },
  isRewardedAdReady: function isRewardedAdReady(adUnitId) {
    var isReady = this.isAdReadyValues[adUnitId];
    return typeof isReady === 'boolean' && isReady === true;
  },
  showRewardedAd: function showRewardedAd(adUnitId, placement) {
    var parameters = [adUnitId];

    if (placement != null) {
      parameters.push(placement);
    }

    callNative('showRewardedAd', parameters);
  },
  setRewardedAdExtraParameter: function setRewardedAdExtraParameter(adUnitId, key, value) {
    callNative('setRewardedAdExtraParameter', [adUnitId, key, value]);
  }
}; // Attach listeners for ad readiness state management

window.addEventListener('OnInterstitialLoadedEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = true;
});
window.addEventListener('OnInterstitialLoadFailedEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = false;
});
window.addEventListener('OnInterstitialDisplayedEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = false;
});
window.addEventListener('OnInterstitialAdFailedToDisplayEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = false;
});
window.addEventListener('OnRewardedAdLoadedEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = true;
});
window.addEventListener('OnRewardedAdLoadFailedEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = false;
});
window.addEventListener('OnRewardedAdDisplayedEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = false;
});
window.addEventListener('OnRewardedAdFailedToDisplayEvent', function (adInfo) {
  AppLovinMAX.isAdReadyValues[adInfo.adUnitId] = false;
});

if ((typeof module === "undefined" ? "undefined" : _typeof(module)) !== undefined && module.exports) {
  module.exports = AppLovinMAX;
}